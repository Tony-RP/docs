# Configuring a continious integration pipeline with CircleCI 2.0 & Bitbucket for Rails and MySQL

### Table of contents:

- Setting up CircleCI with bitbucket
- CircleCI overview
- Sample configuration
- Config walkthrough
- Deploy
- Workflows
- Resources

### 1. Setting up CircleCI with Bitbucket

For this step, you will need a CircleCI account. Visit the CircleCI [signup page](https://circleci.com/signup) and click “Start with Bitbucket”.
You will need to give CircleCI access to your Bitbucket account to run your builds. If you already have a CircleCI account then you can navigate to your [dashboard](dashboard).


![Circle CI Signup](images/1_login.png)


click on sign up with bitbucket. Now choose the team if you belong to multile teams


![Setup Team](images/2_team.png)


Choose the project you want to setup CI (You need to have admin level access to setup repo for CI)


![Choose Repo](images/3_repo_permission.png)


### 2. CircleCI overview


CircleCI uses a YAML file to identify how you want your testing environment set up and what tests you want to run.
On CircleCI 2.0, this file must be called `config.yml` and must be in a hidden folder called `.circleci`. On Mac, Linux, and Windows systems, files and folders whose names start with a period are treated as system files that are hidden from users by default.


> CircleCI uses `.circleci/config.yml` to identify the configuration
> for running diffrent steps of our build


### 3. Sample configuration

*Note that this configuration is specific to ELE SSO project and assumes that your database.yml is like the following*

`database.yml`

```YAML
default: &default
  adapter: mysql2
  encoding: utf8
  pool: 5
  timeout: 5000
  username: <%= ENV['DB_USERNAME'] || 'root' %>
  password: <%= ENV['DB_PASSWORD'] %>
  host: <%= ENV['DB_HOST'] || '127.0.0.1' %>

development:
  <<: *default
  database: sso_development

test:
  <<: *default
  database: sso_test
```

`.circleci/config.yaml`

```YAML
version: 2
jobs:
  build:
    parallelism: 1
    working_directory: ~/sso
    docker:
      - image: circleci/ruby:2.5.1-node-browsers
        environment:
          BUNDLE_JOBS: 3
          BUNDLE_RETRY: 3
          BUNDLE_PATH: vendor/bundle
          RAILS_ENV: test
      - image: library/mysql:5.7.17
        environment:
          MYSQL_ALLOW_EMPTY_PASSWORD: yes
          MYSQL_ROOT_PASSWORD: ''
          MYSQL_DATABASE: sso_test
    steps:
      - checkout

      # Which version of bundler?
      - run:
          name: Which bundler?
          command: bundle -v

      # Restore bundle cache
      - restore_cache:
          keys:
            - sso-bundle-v2-{{ checksum "Gemfile.lock" }}
            - sso-bundle-v2-
      - run:
              name: install dependencies
              command: |
                      sudo apt-get update
                      sudo apt-get install libidn11-dev
                      sudo apt-get install qt5-default libqt5webkit5-dev gstreamer1.0-plugins-base gstreamer1.0-tools gstreamer1.0-x
                      sudo apt-get install cmake

      - run:
          name: Bundle Install
          command:  |
                    bundle update better_errors
                    bundle check || bundle install --jobs=4 --retry=3 --path vendor/bundle --without development production

      # Store bundle cache
      - save_cache:
          key: sso-bundle-v2-{{ checksum "Gemfile.lock" }}
          paths:
            - vendor/bundle

      # Only necessary if app uses webpacker or yarn in some other way
      - restore_cache:
          keys:
            - sso-yarn-{{ checksum "yarn.lock" }}
            - sso-yarn-

      - run:
          name: Yarn Install
          command: yarn install --cache-folder ~/.cache/yarn

      # Store yarn / webpacker cache
      - save_cache:
          key: sso-yarn-{{ checksum "yarn.lock" }}
          paths:
            - ~/.cache/yarn

      - run:
          name: Wait for DB
          command: dockerize -wait tcp://127.0.0.1:3306 -timeout 120s

      - run:
          name: Database setup
          command: bin/rails db:schema:load --trace

      - run:
          name: Bundle audit
          command: bundle exec bundle audit

      # Build fails if coding style guidelines is not followed
      - run:
          name: Rubocop
          command: bundle exec rubocop

      - type: shell
        command: |
          bundle exec rspec --profile 10 \
                            --out test_results/rspec.xml \
                            --format progress \
                            $(circleci tests glob "spec/**/*_spec.rb" | circleci tests split --split-by=timings)

      # Save test results for timing analysis
      - store_test_results:
          path: test_results

  deploy:
    docker:
      - image: buildpack-deps:trusty
    steps:
      - checkout
      - run:
          name: Deploy Master to Heroku
          command: |
            git push https://heroku:$HEROKU_API_KEY@git.heroku.com/$HEROKU_APP_NAME.git master

workflows:
  version: 2
  build-deploy:
    jobs:
      - build
      - deploy:
          requires:
            - build
          filters:
            branches:
              only: master
```


### 4. Config walkthrough


Start with the version.

`version: 2`

This tells CircleCI to use CircleCI version 2.0

Next, add a jobs key. Each job represents a phase in your Build-Test-Deploy process.

In each job, you have the option of specifying a working_directory.


```YAML
version: 2
jobs:
  build:
    parallelism: 1
    working_directory: ~/ele_sso
```

This path will be used as the default working directory for the rest of the job unless otherwise specified.

Directly beneath working_directory, you can specify container images under a docker key.

```YAML
docker:
  - image: circleci/ruby:2.5.1-node-browsers
    environment:
      BUNDLE_JOBS: 3
      BUNDLE_RETRY: 3
      BUNDLE_PATH: vendor/bundle
      RAILS_ENV: test
  - image: library/mysql:5.7.17
    environment:
      MYSQL_ALLOW_EMPTY_PASSWORD: yes
      MYSQL_ROOT_PASSWORD: ''
      MYSQL_DATABASE: sso_test
```

The official Ruby images are tagged to use the latest patch-level version of 2.5.1 and with additional packages installed for NodeJS.

The official MySQL image is specified for use as the database container.

Then, several environment variables are added to connect the application container with the database for testing purposes. The BUNDLE_* environment variables are there to ensure proper caching and improve performance and reliability for installing dependencies with Bundler.

Finally, add several steps within the build job.

Start with checkout so CircleCI can operate on the codebase.

```YAML
steps:
  - checkout
```

This step tells CircleCI to checkout the project code into the working directory.

Next CircleCI pulls down the cache, if present. If this is your first run, or if you’ve changed Gemfile.lock, this won’t do anything. The bundle install command runs next to pull down the project’s dependencies. Normally, you never call this task directly since it’s done automatically when it’s needed, but calling it directly allows a save_cache step that will store the dependencies to speed things up for next time.

```YAML
steps:
  # ...

  # Restore bundle cache
  - restore_cache:
      keys:
        - rails-demo-bundle-v2-{{ checksum "Gemfile.lock" }}
        - rails-demo-bundle-v2-

  - run:
      name: Bundle Install
      command: bundle check || bundle install

  # Store bundle cache
  - save_cache:
      key: rails-demo-bundle-v2-{{ checksum "Gemfile.lock" }}
      paths:
        - vendor/bundle
```

If your application is using Webpack or Yarn for JavaScript dependencies, you should also add the following to your config.

```YAML
steps:
  # ...

  # Only necessary if app uses webpacker or yarn in some other way
  - restore_cache:
      keys:
        - rails-demo-yarn-{{ checksum "yarn.lock" }}
        - rails-demo-yarn-

  - run:
      name: Yarn Install
      command: yarn install --cache-folder ~/.cache/yarn

  # Store yarn / webpacker cache
  - save_cache:
      key: rails-demo-yarn-{{ checksum "yarn.lock" }}
      paths:
        - ~/.cache/yarn
```

The next section sets up the test database. It uses the dockerize utility to delay starting the main process of the primary container until after the database service is available.

```YAML
steps:
  # ...

  # Database setup
  - run:
      name: Wait for DB
      command: dockerize -wait tcp://127.0.0.1:3306 -timeout 120s

  - run:
      name: Database setup
      command: bin/rails db:schema:load --trace
```
Then bundle exec rspec runs the actual tests in parallel.

If they succeed, it stores the test results using store_test_results so CircleCI will quickly show the build failures in the Test Summary section of the app.
From there this can be tied into a continuous deployment scheme of your choice.

### 5. Deploy

The section responsible for pushing our code to heroku is this.

```YAML
deploy:
  docker:
    - image: buildpack-deps:trusty
  steps:
    - checkout
    - run:
        name: Deploy Master to Heroku
        command: |
          git push https://heroku:$HEROKU_API_KEY@git.heroku.com/$HEROKU_APP_NAME.git master
```

We need to add 2 environment variables, HEROKU_API_KEY & HEROKU_APP_NAME in CirclCI.
Refer screenshots.

Adding Heroku API key


![Heroku API Key](images/api_key_var.png)


Adding Heroku App Name


![Heroku API Key](images/app_name_var.png)



### 6. Workflows.

CircleCI workflows allows us to group diffrent jobs( check code quality, run tests, deploy if build is green, etc..) into a workflow. Mainly, a workflow consists of build, and deploy sections

```YAML
workflows:
  version: 2
  build-deploy:
    jobs:
      - build
      - deploy:
          requires:
            - build
          filters:
            branches:
              only: master
```

Note that we are deploting only master branch

### 7. Resources

[Thoughtbot Blog](https://robots.thoughtbot.com/circleci-2-rails)
